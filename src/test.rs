#[cfg(test)]
mod test {
    use rotr;
    use pow3;
    use crz;

    #[test]
    fn it_works() {
        assert_eq!(rotr(1823),39973);
        assert_eq!(pow3(4),81);
        assert_eq!(crz(1131,11355),20650);
    }
}
