use std::fs::File;
use std::io::BufReader;
use std::io::prelude::*;
use std::path::Path;
use std::string::String;

fn main() {
    let args: Vec<String> = std::env::args().collect();
    //If we have no arguments given
    if args.len() != 2 {
        println!("Please give a filename");
        return;
    }
    let filename = &args[1];
    let lines = lines_from_file(&filename);
    let code = lines.join("")
        .replace(" ", "")
        .replace("\n", "")
        .replace("\t", "");
    let mut memory: [i64; 59049] = [0; 59049];
    for (i, b) in code.char_indices() {
        let check = (i as i64 + b as i64) % 94;
        if check != 4 && check != 5 && check != 23 && check != 39 && check != 40 &&
           check != 62 && check != 68 && check != 81 {
            println!("ERROR: Bad code");
            return;
        }
        memory[i] = b as i64;
    }
    for i in code.len()..memory.len() {
        memory[i] = crz(memory[i - 2], memory[i - 1]);
    }
    let mut a: i64 = 0;
    let mut c: i64 = 0;
    let mut d: i64 = 0;
    while (c + memory[c as usize]) % 94 != 81 {
        match (c + memory[c as usize]) % 94 {
            4 => c = d,
            5 => print!("{}", (a as u8) as char),
            23 => {
                let input: Option<i64> = std::io::stdin()
                    .bytes()
                    .next()
                    .and_then(|result| result.ok())
                    .map(|byte| byte as i64);
                a = input.unwrap();
            }
            39 => {
                a = rotr(memory[d as usize]);
                memory[d as usize] = a
            }
            40 => d = memory[d as usize],
            62 => {
                a = crz(memory[d as usize], a);
                memory[d as usize] = a
            }
            _ => {}
        }
        c += 1;
        d += 1;
        memory[(c - 1) as usize] = enc(memory[(c - 1) as usize] % 94);
    }
    println!("\n\nEnd of execution")
}

//Reads a file as a vector of strings
fn lines_from_file<P>(filename: P) -> Vec<String>
    where P: AsRef<Path>
{
    let file = File::open(filename).expect("Error: No such file");
    let buf = BufReader::new(file);
    buf.lines().map(|l| l.expect("Error: Could not parse line")).collect()
}

fn crz(m2: i64, m1: i64) -> i64 {
    let crz_lut: [[i64; 3]; 3] = [[1, 0, 0], [1, 0, 2], [2, 2, 1]];
    let mut out = 0;
    let mut in1 = (m2 / pow3(9)) % 3;
    let mut in2 = (m1 / pow3(9)) % 3;
    for i in (0..9).rev() {
        out *= 3;
        out += crz_lut[in1 as usize][in2 as usize];
        in1 = (m2 / pow3(i)) % 3;
        in2 = (m1 / pow3(i)) % 3;
    }
    out *= 3;
    out += crz_lut[in1 as usize][in2 as usize];
    out
}

fn rotr(int: i64) -> i64 {
    int / 3 + (int % 3) * pow3(9)
}
fn enc(int: i64) -> i64 {
    let enc_lut = [57, 109, 60, 46, 84, 86, 97, 99, 96, 117, 89, 42, 77, 75, 39, 88, 126, 120, 68,
                   108, 125, 82, 69, 111, 107, 78, 58, 35, 63, 71, 34, 105, 64, 53, 122, 93, 38,
                   103, 113, 116, 121, 102, 114, 36, 40, 119, 101, 52, 123, 87, 80, 41, 72, 45,
                   90, 110, 44, 91, 37, 92, 51, 100, 76, 43, 81, 59, 62, 85, 33, 112, 74, 83, 55,
                   50, 70, 104, 79, 65, 49, 67, 66, 54, 118, 94, 61, 73, 95, 48, 47, 56, 124, 106,
                   115, 98];
    enc_lut[int as usize]
}

fn pow3(x: i64) -> i64 {
    (3 as u32).pow(x as u32) as i64
}
